## Kubernetes Teslo-Shop
Practica del curso de docker, Udemy, Fernando Herrera

Implementacion de Teslo-Shop usando kunernetes

### Depedencias
- [minikube](https://minikube.sigs.k8s.io/docs/start/)
- [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/)

### Instalacion 

una vez instaladas la herramientas, he inicializado minikube debemos ejecutar los siguientes comandos:
- Primero aplicamos la configuracion para montar postgress
    - kubectl apply -f postgres-config.yml
    - kubectl apply -f postgres-secrets.yml
    - kubectl apply -f postgres.yml
    - kubectl describe deployment.apps/postgres-deployment
- Segundo aplicamos la configuracion para montar pgadmin
    - kubectl apply -f pgadmin-secrets.yml
    - kubectl apply -f pgadmin.yml
    - kubectl describe deployment.apps/pgadmin-deployment
- Tercero aplicamos la configuracion de la aplicacion perse
    - kubectl apply -f backend-secrets.yml 
    - kubectl apply -f backend.yml 
    - kubectl describe deployment.apps/backend-deployment
- Cuarto, ejecutamos minikube service nombre-del-servicio para abrir la aplicacion y luego ejecutamos url/api/seed para inicializar la base de datos de la aplicacion, donde url es la ruta y puerto que el comando mapeo al servicio. luego podemos probar otros endpoints si accedemos a la documentacion (swagger) accediendo a la ruta url/api


### Otros comandos
- ``kubectl get all``, comando para revisar el status del despliegue.
- ``kubectl logs nombre-del-pod``, comando para revisar el log si encontramos errores, ejemplo: ``kubectl logs pod/pgadmin-deployment-7dbdc8b8f-f4vh5``
- ``minikube service pgadmin-service``, comando para abrir el pgadmin dentro de kubernetes. hace el mapeo de puerto segun se definio, y abre el navegador




